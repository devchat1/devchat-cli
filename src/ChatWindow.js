/**
 * @author Adam Siekierski <adam.siekiera@outlook.com>
 */

const blessed = require('blessed');
const moment = require('moment');

class ChatWindow {
  constructor({ messages, onSend, roomName, user, onClose, plugins }) {
    this.onSend = onSend;
    this.messages = messages;
    this.user = user;
    this.plugins = plugins;

    this.screen = blessed.screen({ smartCSR: true });
    this.screen.title = `DevChat | room: "${roomName}"`;

    this.title = blessed.box({
      parent: this.screen,
      top: 0,
      left: 0,
      width: '100%',
      height: 1,
      tags: true,
      style: {
        bg: 'white',
        fg: 'black',
      },
      content: `{center}${this.user.nickname} | DevChat | room: "${roomName}"{/center}`,
    });

    this.messageBox = blessed.box({
      parent: this.screen,
      scrollbar: true,
      top: 1,
      left: 0,
      width: '100%',
      height: '100%-2',
      tags: true,
      scrollable: true,
      keys: true,
      mouse: true,
      style: {
        scrollbar: {
          bg: 'white',
        },
      },
    });

    this.form = blessed.form({
      parent: this.screen,
      content: '> ',
      bottom: 0,
      left: 0,
      height: 1,
      width: '100%',
      style: {
        fg: 'white',
      },
    });

    this.textInput = blessed.textbox({
      parent: this.form,
      bottom: 0,
      left: 2,
      height: 1,
      width: '100%',
      inputOnFocus: true,
      keys: true,
      style: {
        bg: 'white',
        fg: 'black',
      },
    });

    this.screen.on('keypress', () => this.textInput.focus());

    this.textInput.key(['C-c'], () => {
      onClose({ messages: this.messages });
      process.exit();
    });

    this.textInput.on('submit', () => {
      const value = this.textInput.value;
      this.textInput.clearValue('');
      this.screen.render();
      this.onSend(value);
    });

    this.textInput.key(['up'], () => {
      this.messageBox.scroll(-1);
      this.screen.render();
    });

    this.textInput.key(['down'], () => {
      this.messageBox.scroll(1);
      this.screen.render();
    });

    this.render();
  }

  render() {
    this.messages.map((message, i) => {
      const plugins = this.plugins.filter(plugin => {
        return plugin.config.contentType === message.contentType;
      });

      if (plugins.length === 1) {
        // plugin for this content type found
        plugins[0].onRender(message, this.messageBox, i, this.user);
      } else if (plugins.length > 1) {
        // multiple plugins for this content type found
        this.messageBox.setLine(
          i,
          '{red-fg} Multiple plugins with the same content type found! Please review your devchat config file {/red-fg}',
        );
      } else {
        // no plugins for this content type found
        const authorColor = message.from._id === this.user._id ? 'grey-fg' : 'blue-fg';
        const date = moment(message.time).format('DD-MM-YYYY HH:mm');
        this.messageBox.setLine(
          i,
          `{bold}{${authorColor}}${message.from.nickname}{/${authorColor}}{/bold} {magenta-fg}[${date}]{/magenta-fg}{red-fg} Unsupported content type! {/red-fg}`,
        );
      }
    });

    this.messageBox.setScrollPerc(100);
    this.textInput.focus();
    this.screen.render();
  }

  addMessage(message) {
    this.messages.push(message);
    this.render();
  }
}

module.exports = ChatWindow;
