/**
 * @author Adam Siekierski <adam.siekiera@outlook.com>
 */

const moment = require('moment');

class TextPlugin {
  get config() {
    // Variables that define the plugin
    return {
      name: 'TextPlugin',
      contentType: 'text',
      prefix: '',
    };
  }

  onMessageSent(content) {
    // When message is sent
    return {
      content: content,
      contentType: this.config.contentType,
    };
  }

  onRender(message, messageBox, i, user) {
    // When the message is rendered
    const date = moment(message.time).format('DD-MM-YYYY HH:mm');

    const authorColor = message.from._id === user._id ? 'grey-fg' : 'blue-fg';

    messageBox.setLine(
      i,
      `{bold}{${authorColor}}${message.from.nickname}{/${authorColor}}{/bold} {magenta-fg}[${date}]{/magenta-fg} ${message.content}`,
    );
  }
}

module.exports = TextPlugin;
