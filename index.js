#! /usr/bin/env node

/**
 * @author DevChat Team
 */

const program = require('commander');
const inquirer = require('inquirer');
const axios = require('axios');
const chalk = require('chalk');
const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const io = require('socket.io-client');
const ChatWindow = require('./src/ChatWindow');
const TextPlugin = require('./src/TextPlugin');
const log = console.log;

const URL = 'https://devchat-api.herokuapp.com';
const CONFIG = path.join(os.homedir(), '.devchat/', '.devchatconfig');

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

// Subcommands

program
  .command('login')
  .description('Login to DevChat account')
  .action(() => {
    inquirer
      .prompt([
        {
          type: 'input',
          name: 'email',
          message: 'Enter your DevChat account email',
        },
        {
          type: 'password',
          name: 'password',
          message: 'Enter your DevChat account password',
        },
      ])
      .then(({ email, password }) => {
        log('Logging in...');
        axios
          .post(`${URL}/user/login`, { email, password })
          .then(res => {
            if (res.data.token) {
              fs.outputJson(
                CONFIG,
                {
                  token: res.data.token,
                  email: res.data.user.email,
                  nickname: res.data.user.nickname,
                  _id: res.data.user._id,
                },
                err => {
                  if (err) {
                    log(chalk.red(err));
                  } else {
                    log(chalk.green(`Logged in successfully`));
                  }
                },
              );
            }
          })
          .catch(err => {
            const res = err.response;

            if (res.data.error) {
              log(chalk.red(`Can't log in: ${res.data.error}`));
            }

            if (res.data.errors) {
              log(chalk.red(`Can't log in: `));
              res.data.errors.map(error => {
                log(chalk.red(`${error.param.capitalize()}: ${error.msg.capitalize()}`));
              });
            }
          });
      });
  });

program
  .command('register')
  .description('Register a new DevChat account')
  .action(() => {
    inquirer
      .prompt([
        {
          type: 'input',
          name: 'email',
          message: 'Enter your email adress',
        },
        {
          type: 'input',
          name: 'nickname',
          message: 'Enter your nickname',
        },
        {
          type: 'password',
          name: 'password',
          message: 'Enter your password',
        },
      ])
      .then(({ email, nickname, password }) => {
        log('Registering...');
        axios
          .post(`${URL}/user/register`, { email, password, nickname })
          .then(res => {
            if (res.data.token) {
              log(chalk.green(`Registered in successfully`));
              log('Logging in...');
              fs.outputJson(
                CONFIG,
                {
                  token: res.data.token,
                  email: res.data.user.email,
                  nickname: res.data.user.nickname,
                  _id: res.data.user._id,
                },
                err => {
                  if (err) {
                    log(chalk.red(err));
                  } else {
                    log(chalk.green('Logged in successfully'));
                  }
                },
              );
            }
          })
          .catch(err => {
            const res = err.response;

            if (res.data.error) {
              log(chalk.red(`Can't register: ${res.data.error}`));
            }

            if (res.data.errors) {
              log(chalk.red(`Can't register: `));
              res.data.errors.map(error => {
                log(chalk.red(`${error.param.capitalize()}: ${error.msg.capitalize()}`));
              });
            }
          });
      });
  });

program
  .command('logout')
  .description('Logout from DevChat CLI')
  .action(() => {
    inquirer
      .prompt({ type: 'confirm', name: 'confirm', message: 'Are you sure to logout?' })
      .then(({ confirm }) => {
        if (confirm) {
          fs.remove(CONFIG, err => {
            if (err) {
              log(chalk.red(err));
            } else {
              log(chalk.yellow('Logged out successfully'));
            }
          });
        }
      });
  });

program
  .command('user')
  .description('Check user data')
  .action(() => {
    let config;

    try {
      config = fs.readJsonSync(CONFIG);
    } catch (err) {
      return log(chalk.yellow(`You're not logged in`));
    }

    if (config.token) {
      axios
        .get(`${URL}/user`, { headers: { Authorization: config.token } })
        .then(res => {
          log(
            JSON.stringify(
              {
                email: res.data.user.email,
                nickname: res.data.user.nickname,
                _id: res.data.user._id,
              },
              null,
              2,
            ),
          );
        })
        .catch(() => log(chalk.red('User not logged in')));
    } else {
      log(chalk.yellow(`You're not logged in`));
    }
  });

program
  .command('init [dir]')
  .description('Initialise DevChat in given directory')
  .action(dir => {
    let config;

    try {
      config = fs.readJsonSync(CONFIG);
    } catch (err) {
      return log(chalk.yellow(`You're not logged in`));
    }

    const chatPath = path.join(process.cwd(), dir || '.');
    const configFile = path.join(chatPath, '.devchat/.devchat.config.js');

    if (fs.existsSync(configFile)) {
      log(chalk.red('DevChat is already initialised in this directory'));
    } else {
      axios
        .post(
          `${URL}/rooms/new`,
          {
            title: path.basename(path.resolve()),
          },
          {
            headers: {
              Authorization: `Bearer ${config.token}`,
            },
          },
        )
        .then(res => {
          fs.outputFileSync(
            configFile,
            `module.exports = ${JSON.stringify(
              {
                title: res.data.room.title,
                _id: res.data.room._id,
              },
              null,
              2,
            )}`,
          );
          log(chalk.green(`Initialised room in directory ${chatPath}`));
        });
    }
  });

program
  .command('chat [dir]')
  .description('Launch DevChat in given directory')
  .action(dir => {
    const chatPath = path.join(process.cwd(), dir || '.');

    let user;

    try {
      user = fs.readJsonSync(CONFIG);
    } catch (err) {
      return log(chalk.yellow(`You're not logged in`));
    }

    let config;
    try {
      config = require(path.join(chatPath, '.devchat/.devchat.config.js'));
    } catch {
      return log(chalk.red("DevChat isn't initialised in this directory"));
    }

    let messagesCache;
    try {
      messagesCache = fs.readJSONSync(path.join(chatPath, '.devchat/messages.json'));
    } catch {
      messagesCache = { messages: [] };
    }
    axios
      .get(`${URL}/rooms/${config._id}?number=${messagesCache.messages.length}`, {
        headers: { Authorization: `Bearer ${user.token}` },
      })
      .then(res => {
        let messages = messagesCache.messages;
        if (res.data.messages) messages = [...messagesCache.messages, ...res.data.messages];

        const socket = io.connect(`${URL}?token=${user.token}&room_id=${config._id}`);

        const plugins = [...(config.plugins ? config.plugins : []), new TextPlugin()];

        const window = new ChatWindow({
          messages,
          onSend: content => {
            const commandPrefix = content.split(' ')[0];
            const foundPlugins = plugins.filter(plugin => plugin.config.prefix === commandPrefix);
            if (foundPlugins.length === 1) {
              socket.emit('message sent', foundPlugins[0].onMessageSent(content));
            } else {
              socket.emit(
                'message sent',
                plugins.find(plugin => plugin.config.contentType === 'text').onMessageSent(content),
              );
            }
          },
          onClose: onCloseArgs => {
            fs.outputJSONSync(path.join(chatPath, '.devchat/messages.json'), {
              messages: onCloseArgs.messages,
            });
          },
          roomName: config.title,
          user,
          plugins,
        });

        socket.on('message', message => {
          window.addMessage(message);
        });
      })
      .catch(err => {
        console.log(err);
        console.log(`Error while entering chat: ${chalk.bold.red(err.response.data.error)}`);
      });
  });

program.parse(process.argv);
